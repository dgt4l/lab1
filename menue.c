#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "struct.h"
#include "stdfunc.h"
#include <string.h>
#include "type.h"


char *str_readline() {
    char buf[81] = {};
    char *res = NULL;
    int len = 0;
    int n = 0;
    do {
        n = scanf("%80[^\n]", buf);
        if (n < 0) {
            if (!res) {
                return NULL;
            }
        } else if (n > 0) {
            int chunk_len = strlen(buf);
            int str_len = len + chunk_len;
            res = realloc(res, str_len + 1);
            memcpy(res + len, buf, chunk_len);
            len = str_len;
        } else {
            scanf("%*c");
        }
    } while (n > 0);

    if (len > 0) {
        res[len] = '\0';
    } else {
        res = calloc(1, sizeof(char));
    }

    return res;
}


bool_t menue(struct string** s, char command){
    stdin->_IO_read_ptr = stdin->_IO_read_end;
    switch(command){
        case 'h':
            printf(
                "Hello, choose any command:\n"
                "-h- get help\n"
                "-q- quit \n"
                "-i- read string from stdin\n"
                "-o- print string to stdout\n"
                "-d- delete string\n"
                "-c- concatinate strings\n"
                "-g- get substr from sym[i] to sym[j]\n"
                "-s- substring search\n"
                "-r- create random string\n"
            );
            break;
        case 'q':
            return false;
        case 'i':
            ;
            printf("Enter string\n");
            char* str = str_readline();
            char flag;
            printf(
                "Which type do you want to work with?\n"
                "1 - int\n"
                "2 - float\n"
                "3 - char\n" 
            );
            struct string* new_str;
            scanf("%c", &flag);
            if(flag != '1' && flag != '2' && flag != '3'){
                printf("Error: FlagError\n");
                break;
            }
            if (flag == '1'){
                new_str = from_cstr(str, &Int);
            } else if (flag == '2'){
                new_str = from_cstr(str, &Float);
            } else {
                new_str = from_cstr(str, &Char);
            }
            free_str(*s);
            *s = new_str;
            free(str);
            break;
        case 'o':
            print_str(*s);
            printf("\n");
            break;
        case 'r':
            ;
            size_t len;
            struct symbol_type* type;
            char flag1;
            printf(
                "Which type do you want to work with?\n"
                "1 - int\n"
                "2 - float\n"
                "3 - char\n" 
            );
            scanf("%c", &flag1);
            if(flag1 != '1' && flag1 != '2' && flag1 != '3'){
                printf("Error: FlagError\n");
                break;
            }
            if (flag1 == '1'){
                type = &Int;
            } else if (flag1 == '2'){
                type = &Float;
            } else {
                type = &Char;
            }
            printf(
                "Enter length\n"
            );
            scanf("%lu", &len);
            free_str(*s);
            *s = random_str(len, type);
            break;
        case 'd':
            free_str(*s);
            *s = string_init(0, &Char);
            break;
        case 'c':
            ;
            printf("Enter string\n");
            str = str_readline();
            struct string* str2 = from_cstr(str, (*s)->s_type);
            struct string* res = concat(*s, str2);
            free_str(*s);
            *s = res;
            free_str(str2);
            free(str);
            break;
        case 'g':
            ;
            size_t ind1, ind2, tmp;
            char ans;
            printf("Enter ind1 and ind2\n");
            scanf("%lu%lu", &ind1, &ind2);
            if(ind2 < ind1){
                tmp = ind1;
                ind1 = ind2;
                ind2 = tmp;
            }
            if(ind1 < 0 || ind2 < 0 || ind1 > (*s)-> str_len || ind2 > (*s) -> str_len){
                printf("Error: Invalid index\n");
                break;
            }
            new_str = substr_from_str(*s, ind1, ind2);
            printf(
                "Do you want to save substr to buffer?\n"
                "[Y/n] - yes/no\n"
            );
            scanf("%c", &ans);
            if(ans == 'Y'){
                free_str(*s);
                *s = new_str;
            }
            print_str(new_str);
            printf("\n");
            free_str(new_str);
            break;
        case 's':
            ;
            printf("Enter substr\n");
            char* substr = str_readline();
            struct string* new_substr = from_cstr(substr, (*s)->s_type);
            int flag2;
            printf(
                "Sensetive to register?\n"
                "0 - yes\n"
                "1 - no\n"
            );
            scanf("%d", &flag2);
            int ind = substr_search(*s, new_substr, flag2);
            if(ind < 0){
                printf("Substr not found\n");
            } else {
                printf("Your substr index at %d\n", ind);
            }
            free(substr);
            free_str(new_substr);
            break;
    }
    stdin->_IO_read_ptr = stdin->_IO_read_end;
    return true;   
}