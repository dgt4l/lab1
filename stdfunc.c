#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "struct.h"
#include "type.h"
#include "stdfunc.h"


struct string* string_init(size_t length, struct symbol_type* type){
    struct string* str = (struct string*)malloc(sizeof(struct string));
    str->string = (struct symbol**)malloc(length * sizeof(struct symbol*));
    str->str_len = length;
    str->s_type = type;
    return str;
}

struct symbol* symbol_init(void* sym, size_t size){
    struct symbol* symbol = (struct symbol*)malloc(sizeof(struct symbol));
    symbol->value = malloc(size);
    memcpy(symbol->value, sym, size);
    symbol->size = size;
    return symbol;
}

struct string* from_cstr(char* str, struct symbol_type* type){
    if(str == NULL){
        printf("Error: string null pointer\n");
        abort();
    }
    int str_len = strlen(str);
    struct string* new_str = string_init(str_len, type);
    for (size_t i = 0; i < str_len; i++){
        new_str->string[i] = type->from_char(str[i]); 
    } 
    return new_str;
}

void free_symbol(struct symbol* sym){
    if(sym ->value == NULL){
        printf("Error: sym null pointer\n");
        abort();
    }
    free(sym->value);
    free(sym);
}

void free_str(struct string* str){
    if(str->string == NULL){
        printf("Error: string null pointer\n");
        abort();
    }
    for (size_t i = 0; i < str->str_len; i++){
        free_symbol(str->string[i]);
    }
    free(str->string);
    free(str);
}

void print_str(struct string* str){
    for (size_t i = 0; i < str->str_len; i++){
        str->s_type->print_str(str->string[i]);
    }
}

struct string* random_str(size_t len, struct symbol_type* type){
    char* str = (char*)malloc((len + 1) * sizeof(char));
    for(size_t i = 0; i < len; i++){
        str[i] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" [rand() %  52];
    }
    str[len] = '\0';
    struct string* new_str = from_cstr(str, type);
    free(str);
    return new_str;
}

void add_symbol(struct string* str, struct symbol* sym){
    if(str->string == NULL){
        printf("Error: string null pointer\n");
        abort();
    }
    if(sym ->value == NULL){
        printf("Error: sym null pointer\n");
        abort();
    }
    str->string = realloc(str->string, (str->str_len + 1) * sizeof(struct symbol*));
    str->string[str->str_len] = sym;
    str->str_len = str->str_len + 1;
}

struct string* concat(struct string* s1, struct string* s2){
    if(s1->string == NULL || s2->string == NULL){
        printf("Error: string null pointer\n");
        abort();
    }
    if(strcmp(s1->s_type->type_name, s2->s_type->type_name) != 0){
        printf("Error: TypeError operation concat not supported with different types\n");
        abort();
    }
    size_t len1 = s1->str_len;
    size_t len2 = s2->str_len;
    struct string* res = string_init(s1->str_len + s2->str_len, s1->s_type);
    size_t j = 0;
    for(size_t i = 0; i < s1->str_len; i++){
        res->string[j++] = symbol_init(s1 -> string[i]->value, s1->string[i]->size);
    }
    for(size_t i = 0; i < s2->str_len; i++){
        res->string[j++] = symbol_init(s2 -> string[i]->value, s2->string[i]->size);
    }
    return res;
}

struct string* substr_from_str(struct string* s, size_t ind1, size_t ind2){
    if(ind1 >= s->str_len){
        printf("Index Error\n");
        abort();
    };
    int j = 0;
    struct string* res = string_init(ind2-ind1, s->s_type);
    for(size_t i = ind1; i < ind2; i++){
        res->string[j] = symbol_init(s->string[i]->value, s->string[i]->size);
        j++;
    }
    return res;
} 

int substr_search(struct string* s, struct string* substr, int flag){
    if(substr == NULL){
        printf("Error: string null pointer\n");
        abort();
    }
    struct symbol_type* type = s->s_type;
    struct symbol a = { 
        malloc(s->string[0]->size), s->string[0]->size  
    };
    struct symbol b = {
        malloc(a.size), a.size
    };
    for(size_t i = 0; i < s->str_len - substr->str_len + 1; i++){
        int flag1 = 0;
        for(size_t j = 0; j < substr->str_len; j++){
            if(flag == 0){
                if(type->comparator(s->string[i + j], substr->string[j]) != 0){
                    flag1 = 1;
                    break;
                }
            } else{
                type->to_lower(s->string[i + j], &a);
                type->to_lower(substr->string[j], &b);
                if(type->comparator(&a, &b) != 0){
                    flag1 = 1;
                    break;
                }
            } 
        }
        if(!flag1){
            free(a.value);
            free(b.value);
            return i;
        }
    }
    free(a.value);
    free(b.value);
    return -1;
}