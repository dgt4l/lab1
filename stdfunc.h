#pragma once
#include <stdio.h>
#include "struct.h"


struct string* string_init(size_t length, struct symbol_type* type);
struct symbol* symbol_init(void* sym, size_t size);
struct string* from_cstr(char* str, struct symbol_type* type);
void free_symbol(struct symbol* sym);
void free_str(struct string* str);
void print_str(struct string* str);
void read_str(struct string* str);
void add_symbol(struct string* str, struct symbol* sym);
struct string* concat(struct string* s1, struct string* s2);
struct string* substr_from_str(struct string* s, size_t ind1, size_t ind2);
int substr_search(struct string* s, struct string* substr, int flag);
struct string* random_str(size_t len, struct symbol_type* type);