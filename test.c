#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "struct.h"
#include "stdfunc.h"
#include "type.h"


int main(){
    struct string* primary_str = from_cstr("abcdefgAbcdzfg", &Char);
    struct string* concatstr = from_cstr("adidas", &Char);
    struct string* substr1 = from_cstr("fgabcd", &Char);
    struct string* substr2 = from_cstr("zfg", &Char);
    struct string* substr3 = from_cstr("abc", &Char);
    struct string* substr4 = from_cstr("gay", &Char);
    size_t ind1 = 0;
    size_t ind2 = 1;
    size_t ind3 = 2;
    size_t ind4 = 5;
    size_t ind5 = 12;
    size_t ind6 = 14;
    printf("Starting Concat_test\n");
    struct string* concat_ans = concat(primary_str, concatstr);
    //if(concat_ans->string != from_cstr("abcdefgAbcdefgadidas", &Char) ->string){
    //    printf("Test Failed\n");
    //    abort();
    //}
    for(size_t i = 0; i < concat_ans->str_len; i++){
        if(concat_ans->s_type->comparator(concat_ans->string[i], from_cstr("abcdefgAbcdzfgadidas", &Char)->string[i]) != 0){
            printf("Test Failed\n");
            abort();
        }
    }
    printf("Concat_test was successfully passed\n");
    printf("Starting Getsubstr_test\n");
    struct string* getsubstr1_ans = substr_from_str(primary_str, ind1, ind2);
    if(getsubstr1_ans->s_type->comparator(getsubstr1_ans->string[0], from_cstr("a", &Char)->string[0]) != 0){
        printf("Test Failed\n");
        abort();
    }
    struct string* getsubstr2_ans = substr_from_str(primary_str, ind3, ind4);
    for(size_t i = 0; i < getsubstr2_ans->str_len; i++){
        if(getsubstr2_ans->s_type->comparator(getsubstr2_ans->string[i], from_cstr("cde", &Char)->string[i]) != 0){
            printf("Test Failed\n");
            abort();
        }
    }
    struct string* getsubstr3_ans = substr_from_str(primary_str, ind5, ind6);
    for(size_t i = 0; i < getsubstr3_ans->str_len; i++){
        if(getsubstr3_ans->s_type->comparator(getsubstr3_ans->string[i], from_cstr("fg", &Char)->string[i]) != 0){
            printf("Test Failed\n");
            abort();
        }
    }
    printf("Getsubstr_test was successfully passed\n");
    printf("Starting Substringsearch_test\n");
    int substrsearch_ans1 = substr_search(primary_str, substr1, 0);
    if(substrsearch_ans1 != -1){
        printf("Test failed\n");
        abort();
    }
    int substrsearch_ans2 = substr_search(primary_str, substr1, 1);
    if(substrsearch_ans2 != 5){
        printf("Test failed\n");
        abort();
    }
    int substrsearch_ans3 = substr_search(primary_str, substr2, 0);
    if(substrsearch_ans3 != 11){
        printf("Test failed\n");
        abort();
    }
    int substrsearch_ans4 = substr_search(primary_str, substr3, 0);
    if(substrsearch_ans4 != 0){
        printf("Test failed\n");
        abort();
    }
    int substrsearch_ans5 = substr_search(primary_str, substr4, 0);
    if(substrsearch_ans5 != -1){
        printf("Test failed\n");
        abort();
    }
    printf("Substringsearch_test was successfully passed\n");
    printf("All Tests done!\n");
    return 0;
}
