CC=gcc
CFILES=main.c stdfunc.c menue.c type.c 
CFLAGS= -g 
OFILE=LAB1
OTESTFILE=ctest
CTESTFILES=test.c stdfunc.c type.c
all:
	${CC} ${CFILES} ${CFLAGS} -o ${OFILE}
test:
	${CC} ${CTESTFILES} ${CFLAGS} -o ${OTESTFILE}
clean:
	rm ${OFILE}