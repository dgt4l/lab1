#pragma once

typedef int bool_t;
#define true 1
#define false 0
#include <stddef.h>


struct symbol{
    void* value;
    size_t size;
};

struct symbol_type{
    char* type_name;
    void (*print_str)(struct symbol*);
    struct symbol* (*from_char)(char sym);
    int (*comparator)(struct symbol*, struct symbol*);
    void (*to_lower)(struct symbol*, struct symbol*);
};

struct string{
    struct symbol** string;
    size_t str_len;
    struct symbol_type* s_type;
};