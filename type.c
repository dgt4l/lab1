#include "stdfunc.h"
#include "struct.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


void int_print(struct symbol* sym){
    printf("%c", *(int*)(sym->value));
}

struct symbol* int_from_char(char sym){
    int tmp = sym;
    struct symbol* new_sym = symbol_init(&tmp, sizeof(int));
    return new_sym;
}

int int_comparator(struct symbol* sym1, struct symbol* sym2){
    int tmp1, tmp2;
    tmp1 = *(int*)sym1->value;
    tmp2 = *(int*)sym2->value;
    if (tmp1 < tmp2){
        return -1;
    } else if (tmp1 > tmp2){
        return 1;
    } else{
        return 0;
    }
}

void int_to_lower(struct symbol* sym1, struct symbol* sym2){
    *(int*)sym2->value = tolower(*(int*)sym1->value); 
}

struct symbol_type Int = {
    "Int",
    int_print,
    int_from_char,
    int_comparator,
    int_to_lower

};

void float_print(struct symbol* sym){
    printf("%c", *(int*)(sym->value));
}

struct symbol* float_from_char(char sym){
    int tmp = sym;
    float tmp1;
    memcpy(&tmp1, &tmp, sizeof(int));
    struct symbol* new_sym = symbol_init(&tmp1, sizeof(float));
    return new_sym;
}

int float_comparator(struct symbol* sym1, struct symbol* sym2){
    float tmp1, tmp2;
    tmp1 = *(float*)sym1->value;
    tmp2 = *(float*)sym2->value;
    if (tmp1 < tmp2){
        return -1;
    } else if (tmp1 > tmp2){
        return 1;
    } else{
        return 0;
    }
}

void float_to_lower(struct symbol* sym1, struct symbol* sym2){  
    *(float*)sym2->value = tolower((int)*(float*)sym1->value);
}

struct symbol_type Float = {
    "Float",
    float_print,
    float_from_char,
    float_comparator,
    float_to_lower
};

void char_print(struct symbol* sym){
    printf("%c", *(char*)(sym->value));
}

struct symbol* char_from_char(char sym){
    struct symbol* new_sym = symbol_init(&sym, sizeof(char));
    return new_sym;
}

int char_comparator(struct symbol* sym1, struct symbol* sym2){
    char tmp1, tmp2;
    tmp1 = *(char*)sym1->value;
    tmp2 = *(char*)sym2->value;
    if (tmp1 < tmp2){
        return -1;
    } else if (tmp1 > tmp2){
        return 1;
    } else{
        return 0;
    }
}

void char_to_lower(struct symbol* sym1, struct symbol* sym2){
    *(char*)sym2->value = tolower(*(char*)sym1->value);
}

struct symbol_type Char = {
    "Char",
    char_print,
    char_from_char,
    char_comparator,
    char_to_lower
};