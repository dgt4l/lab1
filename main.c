#include <stdio.h>
#include "menue.h"
#include "stdfunc.h"
#include "struct.h"
#include <stdlib.h>
#include "type.h"


int main(){
    struct string* primary_str = string_init(0, &Char);
    char command;
    while(scanf("%c", &command) != EOF && menue(&primary_str, command));
    free_str(primary_str);
    return 0;
}